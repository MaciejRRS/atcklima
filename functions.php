<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';




/* Enable support for custom logo. */
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );


register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'atcklima' ),
) );






function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_deregister_script('jquery');
wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.min.js', array(), null, true);
wp_enqueue_script('popper', get_template_directory_uri() . '/assets/src/js/libr/popper.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );


// add scripts on specific webpages
global $template;

	if ( ( basename( $template ) === 'o-nas.php' ) ) {
    wp_enqueue_style( 'style-fancybox', get_template_directory_uri() . '/assets/src/css/jquery.fancybox.min.css' );
    wp_enqueue_script( 'js-fancybox', get_template_directory_uri() . '/assets/src/js/jquery.fancybox.min.js', array(), null, true );
}

}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');












// Register thumbnails
	add_theme_support( 'post-thumbnails' );
    add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode
    
//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Przyciski');
    acf_add_options_page('Szybki kontakt');
    acf_add_options_page('Czyszczenie klimatyzacji - informacja');
	acf_add_options_page('strona - 404');
	acf_add_options_page('Stopka');
}


// delete p from contact form 7
add_filter('wpcf7_autop_or_not', '__return_false');

//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);


// Dodawanie nowego typu postów Usługi (Realizacje) START 

function create_realizacje_posttype(){

    $labels = array(
			'name' => __('Realizacje'),
			'singular_name' => __('Realizacje'),
			'add_new' => __('Dodaj Realizacje'),
			'add_new_item' => __('Dodaj Nową Realizację'),
			'edit_item' => __('Edytuj Realizacje'),
			'new_item' => __('Nowa Realizacja'),
			'view_item' => __('Zobacz Realizację'),
			'search_items' => __('Szukaj Realizacji'),
			'not_found' => __('Nie Znaleziono Realizacji'),
			'not_found_in_trash' => __('Brak Realizacji w koszu'),
			'all_items' => __('Wszystkie Realizacje'),
			'archives' => __('Zarchiwizowane Realizacje'),
			'insert_into_item' => __('Dodaj do Realizacji'),
			'uploaded_to_this_item' => __('Sciągnięto do bieżącej Realizacji'),
			'featured_image' => __('Zdjęcie Realizacji'),
			'set_featured_image' => __('Ustaw Zdjęcie Realizacji'),
			'remove_featured_image' => __('Usuń Zdjęcie Realizacji'),
			'use_featured_image' => __('Użyj Zdjęcie Realizacji'),
			'menu_name' => __('Realizacje')
    );


	$args = array(
		'label' => 'Realizacje',
		'labels' => $labels,
		'description' => __('Typ Postu zawiera treść dla Realizacje.'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 18,
		'menu_icon' => 'dashicons-screenoptions',
		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes', 'custom-fields'),
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array('slug'=>'realizacje','with_front'=>true),
		'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		),
	);
	register_post_type('realizacje', $args);
}

add_action('init','create_realizacje_posttype', 0);

function register_taxonomy_category() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Category'),
        ];

        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
		'show_ui'           => true,
		'label'             => 'My Taxonomy',
        'show_admin_column' => true,
		'query_var'         => true,
		'show_ui' => true,
		'show_in_rest' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
        'rewrite'           => ['slug' => 'category1'],
        ];

        register_taxonomy('category1', ['realizacje'], $args);
}
add_action('init', 'register_taxonomy_category');
// Dodawanie nowego typu postów END




// Dodawanie nowego typu postów Usługi (Oferta) START 

function create_oferty_posttype(){

    $labels = array(
			'name' => __('Oferty'),
			'singular_name' => __('Oferta'),
			'add_new' => __('Dodaj Ofertę'),
			'add_new_item' => __('Dodaj Nową Ofertę'),
			'edit_item' => __('Edytuj Ofertę'),
			'new_item' => __('Nowa Oferta'),
			'view_item' => __('Zobacz Ofertę'),
			'search_items' => __('Szukaj Oferty'),
			'not_found' => __('Nie Znaleziono Oferty'),
			'not_found_in_trash' => __('Brak Oferty w koszu'),
			'all_items' => __('Wszystkie Oferty'),
			'archives' => __('Zarchiwizowane Oferty'),
			'insert_into_item' => __('Dodaj do Oferty'),
			'uploaded_to_this_item' => __('Sciągnięto do bieżącej Oferty'),
			'featured_image' => __('Zdjęcie oferty'),
			'set_featured_image' => __('Ustaw Zdjęcie Oferty'),
			'remove_featured_image' => __('Usuń Zdjęcie Oferty'),
			'use_featured_image' => __('Użyj Zdjęcie Oferty'),
			'menu_name' => __('Oferty')
    );


	$args = array(
		'label' => 'Oferty',
		'labels' => $labels,
		'description' => __('Typ Postu zawiera treść dla Oferty.'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 18,
		'menu_icon' => 'dashicons-screenoptions',
		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes', 'custom-fields'),
		'has_archive' => false,
		'hierarchical' => true,
		'rewrite' => array('slug'=>'oferta','with_front'=>true),
		'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		),
	);
	register_post_type('oferty', $args);
}

add_action('init','create_oferty_posttype', 0);



// Dodawanie nowego typu postów END



// Custom login page
function login_p() { ?>
<style type="text/css">
#login h1 a,
.login h1 a {
    background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/bg/logotyp.svg');
    background-size: contain !important;
    width: 320px;
    max-width: 100%;
    background-size: 320px auto;
    background-repeat: no-repeat;
    padding-bottom: 0px;
}

body {
    background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/bg/home-bg.jpg') !important;
}

#loginform {
    background: rgba(255, 255, 255, .7);
    color: #fff;
    font-family: 'Maven Pro', sans-serif;
    border: none;
}

.button-primary {
    background: #1A92CC !important;
    padding: 0 30px !important;
    color: #fff;
    text-decoration: none;
    font-family: 'Lato', sans-serif;
    font-weight: 400;
    text-transform: uppercase;
    transition: .3s;
    font-size: 27px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
    border-radius: 0 !important;
    border: 0 !important;
}

.button-primary:hover {
    background: #FF3600 !important;
    color: #FFF !important;
}

.privacy-policy-link {
    color: #aaa !important;
}

.login form .input,
.login input[type=password],
.login input[type=text] {
    border-radius: 0;
}

input[type=checkbox]:focus,
input[type=color]:focus,
input[type=date]:focus,
input[type=datetime-local]:focus,
input[type=datetime]:focus,
input[type=email]:focus,
input[type=month]:focus,
input[type=number]:focus,
input[type=password]:focus,
input[type=radio]:focus,
input[type=search]:focus,
input[type=tel]:focus,
input[type=text]:focus,
input[type=time]:focus,
input[type=url]:focus,
input[type=week]:focus,
select:focus,
textarea:focus {
    border-color: #1A92CC !important;
    box-shadow: 0 0 0 1px #1A92CC !important;
    outline: 2px solid transparent !important;
}

.login label {
    color: #505050;
}

.login #backtoblog a,
.login #nav a {
    text-decoration: none;
    color: #555d66;
    text-align: center;
    display: block;
}
</style>
<?php }
		add_action( 'login_enqueue_scripts', 'login_p' );


	

		// contact form 7 and rCaptcha only on specific pages
add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
function deregister_cf7_javascript() {
    if ( !is_page(array(18)) ) {
        wp_deregister_script( 'contact-form-7' );
		wp_deregister_script('google-recaptcha');
    }
}
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
    if ( !is_page(array(18)) ) {
        wp_deregister_style( 'contact-form-7' );
    }
}