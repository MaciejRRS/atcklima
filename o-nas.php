<?php 
/* 
Template Name: O nas
*/ 
?>

<?php get_header() ?>

<main id="about-us"
    class="wrapper">
    <section class="intro-subpage-area">
        <div style="background-image: url(<?php the_field('intro_tlo_subpage') ?>)"
            class="intro-bg-area">
            <div class="row center-column-intro-apla">
                <div class="col-sm-12 col-md-10">
                    <div class="intro-apla-bg">
                        <div class="titleIntro-area">
                            <h1><?php the_field('intro_title_subpage') ?></h1>
                        </div>
                        <div class="textIntro-area">
                            <?php the_field('text_intro_subpage') ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="arrow-animate"><a href="#certificate"><img
                        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
            </div>
        </div>
    </section>

    <section class="seo-text-home">
        <div class="container">
            <div class="about-text">
                <h3> <?php the_field("about_header_dyst");?> </h3>
                <img src="<?php the_field("about_logo_dyst");?>">
            </div>
        </div>
    </section>

    <section id="certificate"
        class="certificate">
        <div class="container">
            <div class="cert-wrap-text">
                <h2><?php the_field('title_section_about_us_cert') ?></h2>
                <?php the_field('text_under_title_about_us_cert') ?>
            </div>
            <a data-fancybox="gallery"
                href="<?php the_field('button_display_cert_about_us') ?>"
                class="btn-blue"><?php the_field('button_name_cert_about_us') ?></a>

        </div>
    </section>
    <div class="dividier"></div>

    <section class="presetation-list">
        <div class="container">
            <div class="wrap-list-presentation">
                <?php
if( have_rows('presentation_list_about_us') ):
    while( have_rows('presentation_list_about_us') ) : the_row(); ?>
                <div class="row justify-content-center nth-row-list-presentation">
                    <div class="col-lg-5 nth-list-presentation-col">
                        <div class="text-block-presentationList">
                            <h3><?php the_sub_field('title_block_presentation') ?></h3>
                            <?php the_sub_field('text_block_presentation') ?>
                        </div>
                    </div>
                    <div class="col-lg-5 nth-list-presentation-col">
                        <div class="image-blockpresentationList">
                            <img src="<?php the_sub_field('img_block_presentation') ?>">
                        </div>
                    </div>
                </div>

                <?php    
            endwhile;
            else :
            endif;
            ?>
            </div>


        </div>
    </section>

    <section class="intro-bottom">
        <div class="container">
            <div class="dividier"></div>
            <div class="intro-bottom-wrapper">
                <?php the_field('title_last_section_about_us','option') ?>
            </div>
            <div class="dividier min-992-d-none"></div>
        </div>
    </section>
</main>



































<?php get_footer() ?>