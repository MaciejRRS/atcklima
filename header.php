<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        <?php wp_title(); ?>
    </title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>

</head>

<body>

    <header>

        <!-- if homepage add class navbarHome -->
        <?php if( is_front_page() || is_home() ) { ?>
        <nav class="navbar navbar-expand-xl navbar-toggleable-md navbar-inverse fixed-top navbarHome">



            <?php } else { ?>
            <nav class="navbar navbar-expand-xl navbar-toggleable-md navbar-inverse fixed-top nav-subpage">
                <?php } ?>

                <div class="container container-custom">
                    <!-- custom logo start -->
                    <?php 
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                     $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    ?>
                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"
                        title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"> <img
                            src="<?php echo $image[0]; ?>" alt=""></a>
                    <!--  custom logo stop -->

                    <!-- Collapse button burger menu Start-->
                    <button class="navbar-toggler first-button" type="button" data-toggle="collapse"
                        data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent20" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <div class="animated-icon1"><span></span><span></span><span></span></div>
                    </button>
                    <!-- Collapse button burger menu End-->
                    <!-- start menu primary  -->
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'link_before'    => '<span class="screen-reader-text">',
                        'link_after'     => '</span>',
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            =>  new WP_Bootstrap_Navwalker(),
                    ) );
                ?>
                    <!-- end menu primary  -->

                </div>
            </nav>
    </header>

    <div class="phone-mobile-fast-contact">
        <a href="tel:<?php the_field('icon_fast_contact_number','option') ?>"><img
                src="<?php the_field('icon_fast_contact','option') ?>"></a>
    </div>

    <div class="number-desctop">
        <a
            href="tel:<?php the_field('icon_fast_contact_number','option') ?>"><?php the_field('icon_fast_contact_number','option') ?></a>
    </div>

    </div>