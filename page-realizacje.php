<?php 
/* 
Template Name: Strona z realizacjami
*/ 
?>


<?php get_header() ?>
<section class="intro-subpage-area">
    <div style="background-image: url(<?php the_field('intro_tlo_subpage') ?>)" class="intro-bg-area">
        <div class="row center-column-intro-apla">
            <div class="col-sm-12 col-md-10">
                <div class="intro-apla-bg">
                    <div class="titleIntro-area">
                        <h1><?php the_field('intro_title_subpage') ?></h1>
                    </div>
                    <div class="textIntro-area">
                        <?php the_field('text_intro_subpage') ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="arrow-animate"><a href="#realization-blocks-grid"><img
                src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
    </div>
</section>


<section id="realization-blocks-grid" class="realizations">
    <div class="container">
        <div class="title-section">
            <h2><?php the_field('tytul_title_section_realziation_homepage') ?></h2>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12 col-xl-10">
                <div class="grid-container">
                    <?php

$custom_query = new WP_Query( 
    array(
    'post_type' => 'realizacje',
    'post_status'=>'publish',
    'posts_per_page' => -1,
    ) 
);
$i = 1;
?>

                    <?php if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>


                    <?php
                    if ($i > 4) {
                       $i = 1; 
                    }
                    ?>

                    <?php $realization_excerpt = get_the_excerpt($post->ID);?>
                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>



                    <!-- start block grid item -->
                    <a class="link-post item<?php echo $i++; ?>" href="<?php echo get_permalink(); ?>">
                        <div style="background: url('<?php echo $url ?>');" class="bg-block-realization">
                        </div>

                        <div class="text-post-apla">
                            <h3><?php the_title(); ?></h3>
                            <?php echo '<p>'.$realization_excerpt.'</p>'; ?>
                        </div>
                    </a>
                    <!-- end block grid item -->


                    <?php
endwhile; 
endif; 
wp_reset_query();
?>


                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>