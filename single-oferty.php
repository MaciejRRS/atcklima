<?php get_header() ?>


<main id="read-text-page">

    <section class="intro-subpage-area">
        <div class="intro-bg-area" style="background-image: url(<?php the_field('cover_zdjecie_w_tle_all_posts') ?>)">
            <div class="row center-column-intro-apla">
                <div class="col-sm-12 col-md-10">
                    <div class="intro-apla-bg">
                        <div class="titleIntro-area">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="textIntro-area">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="arrow-animate"><a href="#text-post"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
        </div>
    </section>




    <section id="text-post" class="text-post-area">
        <div class="container">

            <div class="repeate-section-area">
                <?php
if( have_rows('sekcja_powtarzalna_offer') ):
    while( have_rows('sekcja_powtarzalna_offer') ) : the_row(); ?>
                <div class="repeat_section_catalog">
                    <div class="title-section">
                        <h2><?php the_sub_field('tytul_sekcji_offer_repeate_section') ?></h2>
                    </div>
                    <div class="row justify-content-center">
                        <?php

if( have_rows('lista_plikow_offer_catalog') ):
    while( have_rows('lista_plikow_offer_catalog') ) : the_row(); ?>
                        <div class="col-6 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                            <div class="block-item-catalog">
                                <?php $img_catalog = get_sub_field('miniaturka_pliku_offer_catalog'); ?>
                                <a href="<?php the_sub_field('plik_do_pobrania_offer_catalog_kopia') ?>"
                                    target="_blank">
                                    <img src="<?php echo esc_url($img_catalog['url']); ?>"
                                        alt="<?php echo esc_attr($img_catalog['alt']); ?>">
                                </a>
                            </div>
                        </div>
                        <?php 
    endwhile;
else :
endif; ?>
                    </div>
                </div>

                <?php 
    endwhile;
else :
endif; ?>
            </div>


            <div class="row justify-content-center">
                <div class="col-xl-8 text-wrap">
                    <?php the_content() ?>

                </div>
            </div>
            <div class="dividier"></div>
        </div>

    </section>

</main>


<?php get_footer(); ?>