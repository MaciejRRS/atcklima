<?php 
/* 
Template Name: kontakt
*/ 
?>

<?php get_header() ?>

<main id="contact" class="wrapper">
    <section class="intro-subpage-area-small">
        <div class="title-area">
            <h1> <?php the_title(); ?> </h1>
        </div>
    </section>
    <section class="list-icons-contact">
        <div class="container">
            <div class="row">
                <?php if( have_rows('lista_ikon_kontaktu') ):
    while( have_rows('lista_ikon_kontaktu') ) : the_row(); ?>
                <div class="col-md-6 col-xl-3 doLewej">
                    <div class="wrap-item-block">
                        <?php if( get_sub_field('wlacz_link_kontakt_ikony') ) { ?>
                        <a class="link-area-wrap"
                            href="<?php the_sub_field('rodzaj_linku_ikon_kontaktu') ?>:<?php  the_sub_field('text_lista_ikon_kontaktu'); ?>">
                            <?php } ?>
                            <div class="icon">
                                <img src="<?php  the_sub_field('ikona_lista_ikon_kontaktu'); ?>">
                            </div>
                            <div class="text-of-icon">
                                <h3><?php  the_sub_field('text_lista_ikon_kontaktu'); ?></h3>
                            </div>
                            <?php if( get_sub_field('wlacz_link_kontakt_ikony') ) { ?>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php endwhile;
else :
endif; ?>
            </div>
        </div>
    </section>

    <section class="mapAndForm">
        <div class="container mapAndFormArea">
            <div class="row">
                <div class="col-xl-6 col-contact-order-1">
                    <div class="title-column">
                        <h3><?php the_field('tytul_nad_mapka_contact') ?></h3>
                    </div>
                    <div class="map-area">

                        <?php the_field('mapka_contact_page') ?>
                    </div>
                </div>
                <div class="col-xl-6 col-contact-order-2">
                    <div class="title-column">
                        <h3><?php the_field('tytul_nad_formularzem_contact') ?></h3>
                    </div>
                    <div class="contact-form-area">
                        <?php echo do_shortcode(get_field('formularz_kontaktowy_shortcode_cf7')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="dividier"></div>
    </section>
</main>



































<?php get_footer() ?>