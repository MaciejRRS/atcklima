<footer id="footer">
    <div class="footer-content">
        <section class="intro-footer">
            <div class="container">
                <div class="intro-area-footer">
                    <h2><?php the_field('intro_-_tytul_footer','option') ?></h2>
                    <h3><?php the_field('intro_podtytul_footer','option') ?></h3>
                    <div class="arrow-animate"><a href="#contact-us"><img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
                    </div>
                </div>
            </div>
        </section>


        <section style="background-image: url(<?php the_field('grafika_w_tle_footer','option') ?>);"
            class="footer-bg-wrap">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 col-xl-10 mx-auto p-0 m-0">

                        <!-- ikony social media -->
                        <div id="contact-us" class="social-icons-footer">
                            <?php if( have_rows('lista_ikon_social_media_footer','option') ):
                                while( have_rows('lista_ikon_social_media_footer','option') ) : the_row(); ?>

                            <?php $iconSocialmedia = get_sub_field('ikona_social_media_footer','option'); ?>
                            <div class="icon-socialmedia-item">
                                <a href="<?php the_sub_field('link_ikona_social_media_footer','option'); ?>">
                                    <img src="<?php echo esc_url($iconSocialmedia['url']); ?>"
                                        alt="<?php echo esc_attr($iconSocialmedia['alt']); ?>">
                                </a>
                            </div>
                            <?php    endwhile;
                    else :
                    endif; ?>
                        </div>
                        <!-- end social icons -->

                        <!-- dane adresowe -->
                        <div class="area-adress-footer">
                            <div class="area-adress-flex-footer">
                                <div class="text-adress-area">
                                    <?php the_field('adres_firmy_footer','option'); ?>
                                </div>


                                <div class="contact-icons-footer">
                                    <?php if( have_rows('lista_danych_kontaktowych_footer','option') ):
                                            while( have_rows('lista_danych_kontaktowych_footer','option') ) : the_row(); ?>


                                    <div class="icon-conact-item">
                                        <a
                                            href="<?php the_sub_field('rodzaj_link_danych_kontaktowych_footer', 'option') ?>:<?php the_sub_field('link_danych_kontaktowych_footer','option'); ?>">
                                            <img
                                                src="<?php the_sub_field('ikona_danych_kontaktowych_footer', 'option') ?>">
                                            <?php the_sub_field('link_danych_kontaktowych_footer', 'option') ?>
                                        </a>
                                    </div>
                                    <?php    endwhile;
                                                else :
                                                endif; ?>
                                </div>
                            </div>

                            <div class="area-menu-footer">
                                <div class="container">
                                    <nav class="nav-footer">
                                        <ul>
                                            <?php
                                            if( have_rows('mapa_strony_lista_zakladek','option') ):
                                                while( have_rows('mapa_strony_lista_zakladek','option') ) : the_row();
                                            ?>
                                            <li><a
                                                    href="<?php the_sub_field('link_zakladki_menu_footer', 'option') ?>"><?php the_sub_field('nazwa_zakladki_menu_footer', 'option') ?></a>
                                            </li>
                                            <?php endwhile;
                                                    else :
                                                    endif;
                                                    ?>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- end dane adresowe -->

                        <div class="area-copyright">
                            <div class="link-to-redrocks">
                                <p>Projekt i wykonanie <a href="https://redrocks.pl/" target="_blank">RedRockS - Strony
                                        Internetowe</a>
                                </p>
                            </div>
                            <div class="link-to-redrocks">
                                <p>© 2022 ATC KLIMA, Wszelkie prawa zastrzeżone</p>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </section>
    </div>
</footer>

<?php wp_footer(); ?>

<script>
jQuery(document).ready(function($) {
    $('.navbar-collapse span').removeClass("screen-reader-text");
});
</script>



</body>

</html>