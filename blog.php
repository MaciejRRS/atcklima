<?php 
/* 
Template Name: Blog
*/ 
?>

<?php get_header() ?>

<main id="blog" class="wrapper">
    <section class="intro-subpage-area">
        <div style="background-image: url(<?php the_field('intro_tlo_subpage') ?>)" class="intro-bg-area">
            <div class="row center-column-intro-apla">
                <div class="col-sm-12 col-md-10">
                    <div class="intro-apla-bg">
                        <div class="titleIntro-area">
                            <h1><?php the_field('intro_title_subpage') ?></h1>
                        </div>
                        <div class="textIntro-area">
                            <?php the_field('text_intro_subpage') ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="arrow-animate"><a href="#blog-list"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
        </div>
    </section>


    <?php
// Protect against arbitrary paged values
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = array(
    'post_type' => 'post',
    'post_status'=>'publish',
    'paged' => $paged,
);
$the_query = new WP_Query($args);
?>
    <?php if ( $the_query->have_posts() ) : ?>


    <section id="blog-list" class="presetation-list">
        <div class="container">
            <div class="wrap-list-presentation">

                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                <div class="row justify-content-center nth-row-list-presentation">
                    <div class="col-lg-5 nth-list-presentation-col">
                        <a href="<?php the_permalink(); ?>">
                            <div class="text-block-presentationList">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                                <span class="read-more"><?php the_field('blog_czytaj_wiecej_text', 'option') ?></span>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-5 nth-list-presentation-col">
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-blockpresentationList">
                                <img src="<?php the_post_thumbnail_url('medium'); ?>">
                            </div>
                        </a>
                    </div>
                </div>

                <?php endwhile; ?>
                <!-- end of the loop -->
                <?php wp_reset_query(); ?>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="pagination">
                        <?php
								echo paginate_links( array(
									'format'  => 'page/%#%',
									'current' => $paged,
									'total'   => $the_query->max_num_pages,
									'mid_size'        => 2,
									'prev_text'       => __('&laquo;  Cofnij'),
									'next_text'       => __('Dalej  &raquo;')
								) );
							?>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
            <div class="dividier max991-d-none"></div>
        </div>
    </section>
    <section class="intro-bottom">
        <div class="container">
            <div class="dividier"></div>
            <div class="intro-bottom-wrapper">
                <?php the_field('title_last_section_about_us','option') ?>
            </div>
            <div class="dividier"></div>
        </div>
    </section>

</main>



































<?php get_footer() ?>