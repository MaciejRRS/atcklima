<?php 
/* 
Template Name: Katalog i Cenniki
*/ 
?>

<?php get_header() ?>


<main id="catalogs"
    class="wrapper">
    <section class="intro-subpage-area-small">
        <div class="title-area">
            <h1> <?php the_title(); ?> </h1>
        </div>
    </section>

    <section class="intro-text">
        <div class="intro-wrap-text">
            <p><?php the_field("desc_catalog");?></p>
        </div>
    </section>

    <section class="katalogi">
        <div class="container">
            <h3> <?php the_field("subtitle_catalog_1");?> </h3>

            <div class="katalogi-lista">


                <?php if( get_field('catalog_catalog') ): $i=0; ?>
                <?php while( the_repeater_field('catalog_catalog') ): $i++; ?>

                <div class="katalog">
                    <a href="<?php the_sub_field('link_price_list'); ?>">
                        <div class="katalog-box">

                            <img class="katalog-image"
                                src="<?php the_sub_field('logo_catalog'); ?>">
                        </div>
                        <p class="katalog-nazwa"><?php the_sub_field('text_catalog'); ?> </p>
                    </a>
                </div>


                <?php endwhile; ?>
                <?php endif; ?>

            </div>
        </div>
    </section>

    <section class="cenniki">
        <div class="container">
            <h3> <?php the_field("subtitle_catalog_2");?> </h3>


            <div class="cenniki-lista">

                <?php if( get_field('catalog_price_list') ): $i=0; ?>
                <?php while( the_repeater_field('catalog_price_list') ): $i++; ?>


                <div class="cenniki">
                    <a href="<?php the_sub_field('link_price_list'); ?>">
                        <div class="cennik-box">
                            <img class="cennik-image"
                                src="<?php the_sub_field('logo_price_list'); ?>">
                        </div>
                    </a>
                    <p class="cenniki-nazwa"><?php the_sub_field('text_price_list'); ?> </p>
                </div>


                <?php endwhile; ?>
                <?php endif; ?>

            </div>

        </div>
    </section>
    <div class="dividier"></div>
</main>

<?php get_footer() ?>