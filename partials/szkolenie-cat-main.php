                <!-- show post category wrap   -->
                <div class="col-xl-4 col-md-6 card__wrap card__parent"
                    href="<?php echo get_permalink(); ?>">
                    <?php $image = get_field('ikona_szkolenia'); ?>
                    <div class="trainings__card card__full card">
                        <p class="card__name"><?php the_title(); ?></p>
                        <p class="card__description">
                            <?php echo wp_trim_words( get_field('opis_kategorii'), 25, '...' ); ?></p>
                        <img src="<?php echo esc_url($image['url']); ?>"
                            alt="<?php echo esc_attr($image['alt']); ?>"
                            class="card__image">
                    </div>
                </div>