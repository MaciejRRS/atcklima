    <?php global $post;
    $postID =  '374' ?>

    <?php
            $args = array(
                'post_type' => 'szkolenia',
                'post_status' => 'publish',
                'category1' => 'skrot',
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
            
            $parent = new WP_Query( $args );
            $count = $parent->found_posts; 
            //var_dump($count);
            
    ?>

    <?php if ($count <= 3 ) { ?>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">
            <?php
            $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
            
            $parent = new WP_Query( $args );
            if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>

            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <?php } else if ($count > 3 && $count <= 6 ) { ?>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">
            <?php
            $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
            
            $parent = new WP_Query( $args );
            if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>

            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 3,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>


    <?php } else if ($count > 6 && $count <= 9 ) { ?>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">
            <?php
            $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
            
            $parent = new WP_Query( $args );
            if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>

            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 3,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 6,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>




    <?php } else if ($count > 9 && $count <= 12 ) { ?>


    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">
            <?php
            $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
            
            $parent = new WP_Query( $args );
            if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>

            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 3,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 6,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <!-- show short posts with parent ID = $postID  -->
    <div class="col-md-6 col-xl-2 card__wrap">
        <div class="row short__row  mx-0">

            <?php
                
                $args = array(
                'post_type' => 'szkolenia',
                'posts_per_page'    => 3,
                'post_status' => 'publish',
                'category1' => 'skrot',
                'offset' => 9,
                'meta_key' => 'wybor_kategorii_rodzica',
                'meta_value' => $postID
            );
             

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <!-- check if posts has parent with the same ID as this post  -->
            <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>


            <a class="col-md-12 card__wrap short px-0"
                href="<?php echo get_permalink() ?>">

                <div class="trainings__card card__short training card short"
                    style="background-color: #f7f7f7; ">
                    <p class="card__name"><?php the_title(); ?></p>
                </div>
            </a>



            <?php endwhile; ?>

            <?php wp_reset_query(); ?>

            <?php endif; ?>

        </div>
    </div>

    <?php } ?>