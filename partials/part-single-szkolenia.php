    <!-- pokaż single post -->
    <section class="training__intro ">
        <div class="container intro">
            <div class="row mx-0 ">

<?php if ($image) { ?>
                <div class="col-lg-6">

                    <?php $image = get_field('ikona_szkolenia'); ?>

                    <img src="<?php echo esc_url($image['url']); ?>"
                        alt="<?php echo esc_attr($image['alt']);?>"
                        class="intro__img">

                </div>
<?php } ?>
                <div class="col-lg-6">
                    <h2 class="intro__title"><?php the_title(); ?></h2>
                    <p class="intro__additional-text"><?php the_field('dodatkowy_pod_tytul') ?></p>
                    <p class="intro__for for"><?php the_field('napis_dla_kogo', 'option'); ?></p>
                    <p class="for__description"><?php the_field('opis_kategorii') ?></p>
                    <a href="#training-form"
                        class="intro__btn"><?php the_field('napis_na_przycisku_zapisz_sie', 'option'); ?></a>
                </div>


            </div>
        </div>
    </section>


    <section class="training__data data">
        <div class="container">
            <div class="row">
                <div class="col-md-4 data__card">
                    <div class=" card">
                        <img src="<?php the_field('ikona_czasu_trwania', 'option'); ?>"
                            alt="<?php the_field('napis_czas_trwania', 'option'); ?>"
                            class="card__icon">
                        <p class="card__name"><?php the_field('napis_czas_trwania', 'option'); ?></p>
                        <p class="card__info"><?php the_field('czas_trwania_szkolenia'); ?></p>
                    </div>
                </div>
                <div class="col-md-4 data__card">
                    <div class=" card">
                        <img src="<?php the_field('ikona_kosztu_szkolenia', 'option'); ?>"
                            alt="<?php the_field('napis_koszt_szkolenia', 'option'); ?>"
                            class="card__icon">
                        <p class="card__name"><?php the_field('napis_koszt_szkolenia', 'option'); ?></p>
                        <p class="card__info"><?php the_field('koszt_szkolenia'); ?></p>
                    </div>
                </div>
                <div class="col-md-4 data__card ">
                    <div class="card">
                        <img src="<?php the_field('ikona_terminu_szkolenia', 'option'); ?>"
                            alt="<?php the_field('napis_termin_szkolenia', 'option'); ?>"
                            class="card__icon">
                        <p class="card__name"><?php the_field('napis_termin_szkolenia', 'option'); ?></p>
                        <p class="card__info"><?php the_field('termin_szkolenia'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php if (get_field('dodatkowe_pole_tekstowe_pod_ikonami')) { ?>
    <section class="info-desc desc">
        <div class="container">
            <div class="desc__content"><?php the_field('dodatkowe_pole_tekstowe_pod_ikonami') ?></div>
        </div>
    </section>
    <?php } ?>

    <section class="training__description description">
        <div class="container">
            <div class="description__content"><?php the_field('opis_w_apli') ?></div>
        </div>
    </section>


    <section class="training__knowledge knowledge">
        <div class="container">
            <div class="knowledge__board board during">
                <p class="board__title"><?php the_field('napis_w_trakcie_szkolenia_omowia', 'option'); ?></p>
                <ul>
                    <?php if (have_rows('w_trakcie_kursu')): $i = 0;
                    while(have_rows('w_trakcie_kursu')): the_row(''); $i++;?>
                    <li class="board__text"><?php the_sub_field('tresc_kursu') ?></li>
                    <?php endwhile; else: endif; ?>
                </ul>
            </div>
            <div class="knowledge__board board after">
                <p class="board__title"><?php the_field('napis_po_ukonczeniu_szkolenia', 'option'); ?></p>
                <ul>
                    <?php if (have_rows('po_ukonczeniu')): $i = 0;
                    while(have_rows('po_ukonczeniu')): the_row(''); $i++;?>
                    <li class="board__text"><?php the_sub_field('wiedza_po_ukonczeniu') ?></li>
                    <?php endwhile; else: endif; ?>
                </ul>
            </div>
        </div>
    </section>



    <section id="training-form"
        class="training__form form">
        <div class="container">
            <?php  echo do_shortcode('[contact-form-7 id="523" title="Single Training"]'); ?>
        </div>
    </section>