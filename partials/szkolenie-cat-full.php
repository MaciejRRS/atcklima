    <?php global $post;
    $postID =  $post->ID;  
 
    ?>

    <!-- show full posts with parent ID = $postID  -->
    <?php
                
                $args = array(
                    'post_type' => 'szkolenia',
                    'category1' => 'full',
                    'post_status' => 'publish',
                    'posts_per_page'    => -1
                    
                );

                $imageCat = get_field('ikona_szkolenia');
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

    <!-- check if posts has parent with the same ID as this post  -->
    <?php $postParent = get_field('wybor_kategorii_rodzica');  ?>
    <?php $postParentID = $postParent->ID; ?>





    <?php if ( $postID == $postParentID ) { ?>

    <a class="col-xl-4 col-md-6 card__wrap"
        href="<?php echo get_permalink(); ?>">
        <?php $image = get_field('ikona_szkolenia'); ?>
        <div class="trainings__card card__full  full training card">
            <?php if ($image) { ?>
            <img src="<?php echo esc_url($image['url']); ?>"
                alt="<?php echo esc_attr($image['alt']); ?>"
                class="card__image"> <?php } else { ?>
            <img src="<?php echo esc_url($imageCat['url']); ?>"
                alt="<?php echo esc_attr($imageCat['alt']); ?>"
                class="card__image alternative"> <?php } ?>

            <p class="card__name"><?php the_title(); ?></p>
            <p class="card__description">
                <?php echo wp_trim_words( get_field('opis_kategorii'), 25, '...' ); ?></p>

        </div>
    </a>

    <?php } ?>

    <?php endwhile; ?>

    <?php wp_reset_query(); ?>

    <?php endif; ?>