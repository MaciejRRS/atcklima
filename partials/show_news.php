<div class="blog_posts">
    <div class="row justify-content-center">
        <?php 
        // the query
        $the_query = new WP_Query( array(
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'post_type' => 'post'
        )); 
?>

        <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>




        <div class="col-md-6 col-xl-4">
            <a href="<?php echo get_permalink(); ?>">
                <div class="blog_post-wrap">

                    <div class="blog-title-area">
                        <h3 class="blog_post-title"><?php the_title(); ?></h3>
                    </div>
                    <div class="blog_post-description">
                        <?php the_excerpt(); ?>
                    </div>
                    <span class="read-more"><?php the_field('blog_czytaj_wiecej_text', 'option') ?></span>
                </div>
            </a>





        </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

        <?php else : ?>
        <p><?php __('No News'); ?></p>
        <?php endif; ?>
    </div>
</div>