<?php 
/* 
Template Name: Strona Główna
*/ 
?>

<?php get_header() ?>

<main id="homepage"
    class="wrapper">
    <section class="slider-home">
        <img class="img-slider"
            src="<?php the_field('slajder_zdjecie_homepage') ?>">
        <div class="about-gradient"></div>

        <div class="slider-text">
            <h2><?php the_field('title_slider-homepage') ?> </h2>
            <div class="arrow-animate"><a href="#about"><img
                        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-white.png"></a>
            </div>
        </div>
    </section>

    <section class="seo-text-home"
        id="about">
        <div class="container">
            <a href="<?php the_field("image_slider_href");?>">
                <div class="about-text">

                    <h3> <?php the_field("subtitle_slider_homepage");?> </h3>
                    <img src="<?php the_field("image_slider_homepage");?>">

                </div>
            </a>
            <div class="text-seo-area">
                <?php the_field('seo_text_top_homepage') ?>
            </div>
        </div>
    </section>

    <section class="aboutus_home">
        <div class="container">

            <div class="title-section">
                <h2><?php the_field('title_section_aboutus_home') ?></h2>
            </div>
            <div class="text-aboutus-home">
                <?php the_field('text_section_aboutus_home') ?>
            </div>
            <div class="buttons-aboutus-home">
                <?php if( get_field('name_btn_aboutus_home_one') ): ?>
                <a href="<?php the_field('link_btn_aboutus_home_one') ?>"
                    class="btn-aboutus"><?php the_field('name_btn_aboutus_home_one') ?></a>
                <?php endif; ?>

                <?php if( get_field('name_btn_aboutus_home_two') ): ?>
                <a href="<?php the_field('link_btn_aboutus_home_two') ?>"
                    class="btn-aboutus"><?php the_field('name_btn_aboutus_home_two') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section
        style="background-image:linear-gradient(to bottom, rgba(101, 205, 255, 0.3), rgba(101, 205, 255, 0.3)), url('<?php the_field('bg_section_offer_home') ?>')"
        class="offer">
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('tytul_sekcji_offer_homepage') ?></h2>
            </div>
            <div class="block-list-offer-homepage">
                <div class="row">
                    <?php 
if( have_rows('zakres_dzialalnosci_-_lista_blokow_offer_homeapge') ):
    while( have_rows('zakres_dzialalnosci_-_lista_blokow_offer_homeapge') ) : the_row(); ?>
                    <div class="col-md-6 col-xl-3">
                        <a href="<?php the_sub_field('link_do_podstrony_list_offer_homepage') ?>">
                            <div class="block-item-offer-section-homepage">
                                <div class="icon">
                                    <img src="<?php the_sub_field('ikona_bloku_item_offer_homepage') ?>">
                                </div>
                                <div class="text-name-offer">
                                    <h3><?php the_sub_field('podpis_item_offer_homepage_copy') ?></h3>
                                </div>
                                <div class="hover-area-text">
                                    <?php the_sub_field('tekst_po_najechaniu_na_blok_offer_list_homepage') ?>
                                    <div class="readmore-text"><?php the_field('oferta_czytaj_wiecej_text', 'option') ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php  
endwhile;
else :
endif;
?>


                </div>
            </div>
        </div>
    </section>
    <section class="realizations">

        <!-- drugi sposób -->
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('tytul_title_section_realziation_homepage') ?></h2>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-12 col-xl-10">
                    <div class="grid-container">
                        <?php

$custom_query = new WP_Query( 
    array(
    'post_type' => 'realizacje',
    'post_status'=>'publish',
    'posts_per_page' => 4,
    ) 
);
$i = 1;
?>

                        <?php if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>


                        <?php
                    if ($i > 4) {
                       $i = 1; 
                    }
                    ?>

                        <?php $realization_excerpt = get_the_excerpt($post->ID);?>
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


                        <?php if( get_field('wlacz_realizacje_na_stronie_glownej') ) { ?>
                        <!-- start block grid item -->
                        <a class="link-post item<?php echo $i++; ?>"
                            href="<?php echo get_permalink(); ?>">
                            <div style="background: url('<?php echo $url ?>');"
                                class="bg-block-realization">
                            </div>

                            <div class="text-post-apla">
                                <h3><?php the_title(); ?></h3>
                                <?php echo '<p>'.$realization_excerpt.'</p>'; ?>
                            </div>
                        </a>
                        <?php } ?>
                        <!-- end block grid item -->


                        <?php
endwhile; 
endif; 
wp_reset_query();
?>


                    </div>
                    <a href="<?php the_field('more_realizations_link_homepage') ?>"
                        class="btn-blue btn-realizations">
                        <?php the_field('more_realizations_name_homepage') ?>
                    </a>
                </div>
            </div>
        </div>
        <!-- end drugi sposób -->
    </section>

    <section
        style="background-image:linear-gradient(to bottom, rgba(0, 172, 255, 0.3), rgba(0, 172, 255, 0.3)), url('<?php the_field('bg_section_news_home') ?>')"
        class="news">
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('title_section_news_homepage') ?></h2>
            </div>
            <!-- add News start -->
            <?php get_template_part( 'partials/show_news', 'page' ); ?>
            <!-- add News end -->
        </div>
    </section>




















</main>



































<?php get_footer() ?>