<?php 
/* 
Template Name: Strona z promocjami
*/ 
?>

<?php get_header() ?>


<main id="read-text-page">

    <section class="intro-subpage-area">
        <div class="intro-bg-area intro-promo" style="background-image: url(<?php the_field('intro_tlo_subpage') ?>)">
            <div class="row center-column-intro-apla">
                <div class="col-sm-12 col-md-10">
                    <div class="intro-apla-bg">
                        <div class="titleIntro-area">
                            <h1><?php the_field('intro_title_subpage'); ?></h1>
                        </div>
                        <div class="textIntro-area">
                            <?php the_field('text_intro_subpage');?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="arrow-animate"><a href="#text-post"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/src/img/arrows/arrow-red.png"></a>
        </div>
    </section>

    <section id="text-post" class="text-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-wrap">
                    <?php the_content() ?>

                </div>
            </div>
            <div class="dividier"></div>
        </div>

    </section>

</main>

<?php get_footer() ?>