<?php 
/* 
Template Name: Podstrona tekstowa
*/ 
?>

<?php get_header() ?>

<main id="subpage-text" class="wrapper">
    <section class="intro-subpage-area-small">
        <div class="title-area">
            <h1> <?php the_title(); ?> </h1>
        </div>
    </section>

    <section id="text-post" class="text-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-wrap p-0">
                    <?php the_content() ?>

                </div>
            </div>
            <div class="dividier"></div>
        </div>

    </section>
</main>



































<?php get_footer() ?>